Qemu-Integration for Cygwin
===========================

Makes Qemu and its man pages transparently accessible.
Unix paths translated to Windows paths if necessary.

Why?
----

In most cases Qemu binaries for Windows are either obtained by the Qemu
distribution from https://qemu.weilnetz.de or self compiled using the MinGW-W64
toolchain.
Necessary dependencies are therefore not included in default Cygwin PATH!

Qemu binaries for Windows can't handle Unix paths and Cygwin links.

Integration approach
--------------------

This integration makes Qemu binaries executable using a wrapper script.
A link to the wrapper is created in /usr/bin for each Qemu binary.

Qemu man pages and firmware descriptors are linked to standard paths.

Install, configuration, integration, uninstall
----------------------------------------------

Install the integration by executing `./install.sh` in qemu-integration source
directory.

Configure integration by adjusting `/etc/qemu-integration.conf` to point to the
directories of your local Qemu installation.
See `/etc/qemu-integration.conf` for configuration examples.

Integrate Qemu into Cygwin by executing `qemu-integration-update.sh` to create
the necessary links in `/usr/bin` and `/usr/share`.
Execute `qemu-integration-update.sh` every time `/etc/qemu-integration.conf`
was modified or the content of your Qemu distribution/installation has changed.

Uninstall the integration by executing `./uninstall.sh` in qemu-integration source
directory.

Environment variables
---------------------

Qemu installed to Cygwins MinGW-W64 sys-roots is auto-detected as well as Qemu
from https://qemu.weilnetz.de installed to its default path.
Configuration in `/etc/qemu-integration.conf` may override these defaults by
setting environment variables.

The provided wrapper processes these environment variables:
* QDEBUG QTRACE
  - If set, enables verbose output
* DISABLE_PATH_TRANSLATION
  - If set, disables path translation
* GTK_CSD
  - If set, influences the title bar style
* WRAPPER_ENV_PATH_EXTENSION
  - Extends wrappers execution PATH
* QEMU_BIN_DIR
  - Defines the Qemu binaries dir and overrides the builtin default.
    Changes require a `qemu-integration-update.sh`
* QEXEC_DUMP
  - Defines a file, to which the wrapped command arguments will be dumped.
    Deactivates execution of the wrapped command!

`qemu-integration-update.sh` processes these environment variables:
* QDEBUG QTRACE
  - If set, enables verbose output
* QEMU_BIN_DIR
  - Defines the Qemu binaries dir and overrides the builtin default.
    Changes require a `qemu-integration-update.sh`
* QEMU_MAN_BASE_DIR
  - Defines the Qemu base man pages dir.
    Changes require a `qemu-integration-update.sh`
* QEMU_FIRMWARE_DIR
  - Defines the Qemu firmware descriptor dir.
    Changes require a `qemu-integration-update.sh`
* DISABLE_INTEGRATION
  - If set to 'true', Qemu integration is removed.
    Changes require a `qemu-integration-update.sh`

