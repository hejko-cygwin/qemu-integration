#!/bin/bash

WRAPPER="qemu-wrapper.sh"

source /usr/share/qemu-integration/functions.sh

function qdirname {
	# OUT: ret_qdirname
	local NAME="$1"
	if [[ $NAME =~ ^(.+)/[^/]+ ]]
	then
		ret_qdirname="${BASH_REMATCH[1]}" ; return
	elif [[ $NAME =~ ^/[^/]+$ ]]
	then
		ret_qdirname="/" ; return
	elif [[ $NAME =~ ^[^/]+$ ]]
	then
		ret_qdirname="." ; return
	fi
	ret_qdirname="" ; return
}

function qbasename {
	# OUT: ret_qbasename
	local NAME="$1"
	if [[ $NAME =~ ([^/]+)$ ]]
	then
		ret_qbasename="${BASH_REMATCH[1]}" ; return
	fi
	ret_qbasename="" ; return
}

function commonPathPrefix {
	# OUT: ret_commonPathPrefix
	local A="$1" B="$2"
	if [ "$A" != "$B" ]
	then
		if (( ${#A} < ${#B} ))
		then
			qdirname "$B" ; B="$ret_qdirname"
			commonPathPrefix "$A" "$B" ; ret_commonPathPrefix="$ret_commonPathPrefix" ; return
		else
			qdirname "$A" ; A="$ret_qdirname"
			commonPathPrefix "$B" "$A" ; ret_commonPathPrefix="$ret_commonPathPrefix" ; return
		fi
	else
		ret_commonPathPrefix="$A" ; return
	fi
}

function qcygpath {
	cygpath -m -l "$1"
}

function qrealpath {
	# OUT: ret_qrealpath
	local FILENAME="$1"
	local FILE="$(realpath "$FILENAME")"
	if (( ${#FILE} >= ${#CYGROOT} )) &&
		[ "${FILE:0:${#CYGROOT}}" == "${CYGROOT}" ]
	then
		ret_qrealpath="${FILE:${#CYGROOT}}" ; return
	else
		ret_qrealpath="$FILE" ; return
	fi
}

function translateExistingPath {
	# OUT: ret_translateExistingPath
	local PARAM="$1"
	debugLog "Existing path to translate: '$PARAM'"
	if [[ $PARAM =~ ^/ ]]
	then
		traceLog "Absolute path"
		if [[ ! $PARAM =~ ^/(dev|proc)(/|$) ]]
		then
			debugLog "Regular path, use cygpath"
			ret_translateExistingPath="$(qcygpath "$PARAM")" ; return
		else
			debugLog "Won't translate special absolute path"
			ret_translateExistingPath="$PARAM" ; return
		fi
	else
		traceLog "Relative path"
		qrealpath "$PARAM" ; local PARAMPATH="$ret_qrealpath"
		local WORKINGDIR="$PWD"
		commonPathPrefix "$WORKINGDIR" "$PARAMPATH" ; local COMMONDIR="$ret_commonPathPrefix"
		debugLog "Paths by realpath: commondir:'$COMMONDIR' - workingdir:'$WORKINGDIR' - path:'$PARAMPATH'"
		# relInHomeOnly: if [[ ! $COMMONDIR =~ ^$HOME ]]
		if [ "$COMMONDIR" == "/" ] && [[ $PARAMPATH =~ ^/cygdrive ]]
		then
			#debugLog "Path is outside of ~/, use cygpath"
			debugLog "Path is outside of relative sight, use cygpath"
			ret_translateExistingPath="$(qcygpath "$PARAM")" ; return
		else
			debugLog "Path is file/dir in working dir or in super of working dir, turn relative"
			local WORKINGDIR_TO_COMMONDIR
			while [ "$WORKINGDIR" != "$COMMONDIR" ]
			do
				qdirname "$WORKINGDIR" ; WORKINGDIR="$ret_qdirname"
				WORKINGDIR_TO_COMMONDIR="../$WORKINGDIR_TO_COMMONDIR"
			done
			traceLog "WORKINGDIR_TO_COMMONDIR: '$WORKINGDIR_TO_COMMONDIR'"
			local RELPATH_START=${#COMMONDIR}
			# RELPATH doesn't start with /, add 1 to jump over / after COMMONDIR prefix
			[ "$COMMONDIR" != "/" ] && RELPATH_START=$(( RELPATH_START+1 ))
			local RELPATH_FROM_COMMONDIR="${PARAMPATH:RELPATH_START}"
			traceLog "RELPATH_FROM_COMMONDIR: '$RELPATH_FROM_COMMONDIR'"
			local RELPATH="${WORKINGDIR_TO_COMMONDIR}${RELPATH_FROM_COMMONDIR}"
			traceLog "RELPATH: '$RELPATH'"
			ret_translateExistingPath="$RELPATH" ; return
		fi
	fi
}

function translatePotentialPath {
	# OUT: ret_translatePotentialPath
	local PARAM="$1"
	traceLog "Potential path to translate: '$PARAM'"
	if [[ $PARAM =~ ^.: ]] || [[ $PARAM =~ \\ ]]
	then
		traceLog "Looks like Windows path, no translation"
		ret_translatePotentialPath="$PARAM" ; return
	elif [[ $PARAM =~ ^~(.*)$ ]]
	then
		local EXPANDED="${HOME}${BASH_REMATCH[1]}"
		debugLog "Starts with '~': '$PARAM', try expanded..."
		translatePotentialPath "$EXPANDED" ; local TEXPANDED="$ret_translatePotentialPath"
		if [ "$EXPANDED" != "$TEXPANDED" ]
		then
			traceLog "Translated to '$TEXPANDED'"
			ret_translatePotentialPath="$TEXPANDED" ; return
		else
			traceLog "Not modified on translation"
			ret_translatePotentialPath="$PARAM" ; return
		fi
	elif [ -e "$PARAM" ]
	then
		debugLog "Is existing file/dir, go on..."
		translateExistingPath "$PARAM" ; ret_translatePotentialPath="$ret_translateExistingPath" ; return
	elif [ ! -L "$PARAM" ] && [[ $PARAM =~ /.+ ]]
	then
		traceLog "Is no dangling link and contains a slash..."
		qdirname "$PARAM" ; local DPARAM="$ret_qdirname"
		qbasename "$PARAM" ; local BPARAM="$ret_qbasename"
		if [ -d "$DPARAM" ]
		then
			debugLog "Path has existing super dir '$DPARAM', go on ..."
			local ODPARAM="$DPARAM"
			translateExistingPath "$DPARAM" ; DPARAM="$ret_translateExistingPath"
			if [ "$ODPARAM" != "$DPARAM" ]
			then
				traceLog "Translated to '$DPARAM/$BPARAM'"
				ret_translatePotentialPath="$DPARAM/$BPARAM" ; return
			else
				traceLog "Not modified on translation"
				ret_translatePotentialPath="$PARAM" ; return
			fi
		else
			traceLog "No translation"
			ret_translatePotentialPath="$PARAM" ; return
		fi
	else
		traceLog "No translation. Param: '$PARAM'"
		ret_translatePotentialPath="$PARAM" ; return
	fi
}

function translateParam {
	# IN: TPARAM
	# OUT: TPARAM
	local PARAM="$TPARAM"
	traceLog "Analyse param for translation: '$PARAM'"
	if [[ $PARAM =~ = ]]
	then
		traceLog "Param contains '=', assuming comma-separated key-value list"
		local REST="$PARAM" TRANS=""
		while [[ $REST =~ ^([^,]+),?(.*) ]]
		do
			local TOKEN="${BASH_REMATCH[1]}" REST="${BASH_REMATCH[2]}"
			if [[ $TOKEN =~ ^(logfile|file|filename|file.filename|path|splash|script|x509-[^=]+)=(.+)$ ]]
			then
				traceLog "Token: '$TOKEN'"
				local TOKEN_NAME="${BASH_REMATCH[1]}" TOKEN_OVALUE="${BASH_REMATCH[2]}"
				translatePotentialPath "$TOKEN_OVALUE" ; local TOKEN_TVALUE="$ret_translatePotentialPath"
				TOKEN="${TOKEN_NAME}=${TOKEN_TVALUE}"
			fi
			TRANS="${TRANS}${TOKEN},"
		done
		TPARAM="${TRANS::-1}"
	else
		traceLog "Assuming plain param"
		traceLog "Param before: '$PARAM'"
		translatePotentialPath "$PARAM"; local TRANS="$ret_translatePotentialPath"
		traceLog "Param after:  '$TRANS'"
		TPARAM="$TRANS"
	fi
	traceLog
}

function parseJsonFailure {
	infoLog
	infoLog "Failed to parse json, please review!"
	infoLog "Successfully parsed:     '$JSONOUT'"
	infoLog "Parser failed here: ===> '$JSONIN'"
	infoLog
	infoLog "Alternatively perform 'export DISABLE_PATH_TRANSLATION=1' before qemu execution."
	exit 1
}

function parsedJsonIn2OutSimple {
	JSONOUT="$JSONOUT${BASH_REMATCH[1]}"
	JSONIN="${BASH_REMATCH[2]}"
}

function parseJsonArray {
	local LASTTOKEN=""
	if [[ $JSONIN =~ ^(\[[$s]*)([$S].*)$ ]]
	then
		# Consumes '['
		parsedJsonIn2OutSimple
		while (true)
		do
			if [[ $JSONIN =~ ^(\][$s]*)([$S].*)$ ]]
			then
				# Consumes ']'
				parsedJsonIn2OutSimple
				return
			elif [[ $JSONIN =~ ^(,[$s]*)([$S].*)$ ]]
			then
				# Consumes ','
				parsedJsonIn2OutSimple
				LASTTOKEN="SEPARATOR"
			elif [ "$LASTTOKEN" != "ELEMENT" ]
			then
				# Consumes '<JSONVALUE>'
				parseJsonValue
				LASTTOKEN="ELEMENT"
			else
				parseJsonFailure
			fi
		done
	else
		parseJsonFailure
	fi
}

function parseJsonRecord {
	local LASTTOKEN="" RECORDPATHSUPER="$RECORDPATH"
	if [[ $JSONIN =~ ^({[$s]*)([$S].*)$ ]]
	then
		# Consumes '{'
		parsedJsonIn2OutSimple
		while (true)
		do
			# 'json:{' may end with '}' without any further char: Optional NoSpace
			if [[ $JSONIN =~ ^(}[$s]*)([$S].*)?$ ]]
			then
				# Consumes '}'
				parsedJsonIn2OutSimple
				return
			elif [[ $JSONIN =~ ^(,[$s]*)([$S].*)$ ]]
			then
				# Consumes ','
				parsedJsonIn2OutSimple
				LASTTOKEN="SEPARATOR"
			elif [ "$LASTTOKEN" != "RECORD" ]
			then
				local RECORDNAME
				# Consumes '"<LITERALVALUE>"'
				parseJsonLiteralValue "parseJsonRecord"
				# set ASAP
				RECORDPATH="$RECORDPATHSUPER.$RECORDNAME"
				if [[ $JSONIN =~ ^(:[$s]*)([$S].*)$ ]]
				then
					# Consumes ':'
					parsedJsonIn2OutSimple
					# Consumes '<JSONVALUE>'
					parseJsonValue
					# reset ASAP
					RECORDPATH="$RECORDPATHSUPER"
					LASTTOKEN="RECORD"
				else
					parseJsonFailure
				fi
			else
				parseJsonFailure
			fi
		done
	else
		parseJsonFailure
	fi
}

function parseJsonNumberValue {
	if [[ $JSONIN =~ ^([0-9]*[$s]*)([$S].*)$ ]]
	then
		# Consumes '[0-9]'
		parsedJsonIn2OutSimple
	else
		parseJsonFailure
	fi
}

function parseJsonBareword {
	if [[ $JSONIN =~ ^(true[$s]*)([$S].*)$ ]] ||
		[[ $JSONIN =~ ^(false[$s]*)([$S].*)$ ]] ||
		[[ $JSONIN =~ ^(null[$s]*)([$S].*)$ ]]
	then
		# Consumes 'true', 'false', 'null'
		parsedJsonIn2OutSimple
	else
		parseJsonFailure
	fi
}

function parseJsonLiteralValue {
	local MODE="$1"
	# TODO '\"'
	if [[ $JSONIN =~ ^(\")([^\"]*)(\"[$s]*)([$S].*)$ ]]
	then
		# Consumes '"<LITERAL>"'
		local PRE="${BASH_REMATCH[1]}" LITERALVALUE="${BASH_REMATCH[2]}" POST="${BASH_REMATCH[3]}"
		JSONIN="${BASH_REMATCH[4]}"
		traceLog "mode:     $MODE"
		traceLog "location: $RECORDPATH"
		if [ "$MODE" == "parseJsonRecord" ]
		then
			# called directly by parseJsonRecord to parse recordname: no translation!
			RECORDNAME="$LITERALVALUE"
		elif [[ $RECORDPATH =~ (logfile|file\.filename|filename)$ ]]
		then
			# real payload, translate for correct $RECORDPATH
			translatePotentialPath "$LITERALVALUE" ; LITERALVALUE="$ret_translatePotentialPath"
		fi
		JSONOUT="${JSONOUT}${PRE}${LITERALVALUE}${POST}"
	else
		parseJsonFailure
	fi
}

function parseJsonValue {
	if [[ $JSONIN =~ ^{ ]]
	then
		parseJsonRecord
	elif [[ $JSONIN =~ ^\[ ]]
	then
		parseJsonArray
	elif [[ $JSONIN =~ ^\" ]]
	then
		parseJsonLiteralValue "parseJsonValue"
	elif [[ $JSONIN =~ ^[0-9] ]]
	then
		parseJsonNumberValue
	elif [[ $JSONIN =~ ^(true|false|null) ]]
	then
		parseJsonBareword
	else
		parseJsonFailure
	fi
}

function translateJson {
	# JSONIN always contains the unprocessed remains of the current parseJsonRecord call
	# JSONOUT always contains the processed json of the current parseJson call
	parseJsonRecord
	traceLog "translateJson: '$JSONPRE'"
	traceLog "translateJson: '$JSONOUT'"
	traceLog "translateJson: '$JSONIN'"
	TJSONS+=("$JSONOUT")
	L=$(( ${#TJSONS[@]}-1 ))
	PARAM="${PRE}_JSONOUT_${L}_${JSONIN}"
}

function extractAndTranslateJsons {
	# IN: TPARAM
	# OUT: TPARAM
	local PARAM="$TPARAM" RECORDPATH PRE JSONOUT JSONIN
	local s='[:space:]' S='^[:space:]' L
	traceLog "before: $PARAM"
	if [[ $PARAM =~ ^({.*)$ ]]
	then
		# transforms PARAM = '{"n1":{"n2":"v",...},...}' into PARAM = 'JSONPARAM_X'
		# and stores translated '{"n1":{"n2":"v",...},...}' in TJSONS[$X] for later concatenation
		PRE="" JSONOUT="" JSONIN="${BASH_REMATCH[1]}"
		translateJson
		# PARAM is expected to be exactly one json parameter and empty after parseJsonRecord
		[ -z "$JSONIN" ] || parseJsonFailure
	else
		# transforms PARAM = '...json:{"n1":{"n2":"v",...},...}...' into PARAM = '...JSONPARAM_X...'
		# and stores translated 'json:{"n1":{"n2":"v",...},...}' in TJSONS[$X] for later concatenation
		while [[ $PARAM =~ ^(.*)(json:)({.*)$ ]]
		do
			PRE="${BASH_REMATCH[1]}" JSONOUT="${BASH_REMATCH[2]}" JSONIN="${BASH_REMATCH[3]}"
			translateJson
		done
	fi
	traceLog "\${#TJSONS[@]}: ${#TJSONS[@]}"
	traceLog "after: $PARAM"
	TPARAM="$PARAM"
}

function integrateTranslatedJsons {
	# IN: TPARAM
	# OUT: TPARAM
	local PARAM="$TPARAM" I=0 L=${#TJSONS[@]}
	traceLog "\${#TJSONS[@]}: $L"
	traceLog "before: $PARAM"
	while (( I < L ))
	do
		if [[ $PARAM =~ ^(.*)_JSONOUT_${I}_(.*)$ ]]
		then
			PARAM="${BASH_REMATCH[1]}${TJSONS[${I}]}${BASH_REMATCH[2]}"
		else
			infoLog "Failure $I/$L"
			exit 1
		fi
		I=$(( I+1 ))
	done
	traceLog "after: $PARAM"
	TPARAM="$PARAM"
}

function infoLines {
	local LINE="$1"
	LINES="${LINES}${LINE}\n"
}

function printTranslated {
	local PARAM LINES LINE INDENT PREVH CURRH
	infoLines
	infoLines "/usr/share/qemu-integration/$WRAPPER applied its path translation:"
	infoLines "--------------------------------------------------------------------------------"
	for PARAM in "${@}"
	do
		[ "${PARAM:0:1}" == '-' ] && CURRH="-" || CURRH=""

		# Newline before "-"-param and between 2 non-"-"-params
		if [ -n "$LINE" ] && ( [ -n "$CURRH" ] || [ -z "$PREVH" ] )
		then
			infoLines "$LINE \\\\"
			LINE=""
		fi

		# Add quots to param, if param is empty or contains blanks or double quots
		if [ -z "$PARAM" ] || [[ $PARAM =~ ' ' ]] || [[ $PARAM =~ '"' ]]
		then
			PARAM="'$PARAM'"
		fi

		LINE="${LINE}${INDENT}${PARAM}"

		INDENT=" "
		PREVH="$CURRH"
	done
	infoLines "$LINE"
	infoLines "--------------------------------------------------------------------------------"
	infoLines
	infoLog -e "$LINES"
}


# Try to determine and translate unix to windows paths 
declare -a QPARAMS=()
declare -a TJSONS
CYGROOT="$(qcygpath "/")"
[[ $CYGROOT =~ ^([^:]+):(.*)$ ]] &&
	CYGROOT="/cygdrive/${BASH_REMATCH[1]}/${BASH_REMATCH[2]}" || CYGROOT=""
TAPPLIED="false"
for QPARAM in "$@"
do
	if [ -z "$DISABLE_PATH_TRANSLATION" ]
	then
		traceLog "Param before: '$QPARAM'"
		TPARAM="$QPARAM"
		traceLog "TJSONS=()"
		TJSONS=()
		extractAndTranslateJsons
		translateParam
		integrateTranslatedJsons
		traceLog "Param after:  '$TPARAM'"
		if [ "$TPARAM" != "$QPARAM" ]
		then
			TAPPLIED="true"
		fi
		QPARAM="$TPARAM"
	fi
	QPARAMS+=("$QPARAM")
done
if [ -z "$DISABLE_PATH_TRANSLATION" ] && $TAPPLIED
then
	qbasename "$0" ; printTranslated "$ret_qbasename" "${QPARAMS[@]}"
fi

# Define native windows title bar if not defined otherwise
if [ -z "$GTK_CSD"]
then
	export GTK_CSD=0
fi

determineConfig

# Add mingw/bin and configured extension to PATH
export PATH="$QEMU_BIN_DIR:$WRAPPER_ENV_PATH_EXTENSION:$PATH"

# Determine binary to execute
qbasename "$0" ; QBINARY="$QEMU_BIN_DIR/$ret_qbasename"
debugLog "$QBINARY"
if [ ! -f "$QBINARY" ]
then
	# Suffix .exe was not used on invocation
	QBINARY="$QBINARY.exe"
	debugLog "$QBINARY"
fi
qexec "$QBINARY" "${QPARAMS[@]}"
