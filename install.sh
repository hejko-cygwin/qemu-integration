#!/bin/bash

cd $(dirname $0)

install -v -m 755 qemu-integration-update.sh /usr/bin/
install -v -d /usr/share/qemu-integration
install -v -m 755 qemu-wrapper.sh /usr/share/qemu-integration/
install -v -m 644 functions.sh /usr/share/qemu-integration/
install -v -m 644 qemu-integration.conf /etc/
qemu-integration-update.sh
