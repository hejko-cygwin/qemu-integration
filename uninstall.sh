#!/bin/bash

[ -f /etc/qemu-integration.conf ] && rm -v /etc/qemu-integration.conf
[ -f /usr/bin/qemu-integration-update.sh ] && export DISABLE_INTEGRATION=true && qemu-integration-update.sh
[ -f /usr/bin/qemu-integration-update.sh ] && rm -v /usr/bin/qemu-integration-update.sh
[ -f /usr/share/qemu-integration/qemu-wrapper.sh ] && rm -v /usr/share/qemu-integration/qemu-wrapper.sh
[ -f /usr/share/qemu-integration/functions.sh ] && rm -v /usr/share/qemu-integration/functions.sh
[ -d /usr/share/qemu-integration ] && rmdir -v /usr/share/qemu-integration
