#!/bin/bash

source $(dirname $(realpath $0))/functions.sh

SYSROOT="/usr/x86_64-w64-mingw32/sys-root/mingw"
DIST="$(cygpath -u "$PROGRAMFILES")/qemu"
REL_STD_TEST_DIR="$(basename $0)"
TMP_TEST_DIR="/tmp/$REL_STD_TEST_DIR"
HOME_TEST_DIR="$HOME/$REL_STD_TEST_DIR"

function setupTest {
	WRAPPER="/usr/share/qemu-integration/qemu-wrapper.sh"
	FUNCTIONS="/usr/share/qemu-integration/functions.sh"
	if [ ! -f "$WRAPPER" ]
	then
		"qemu-integration is not installed"
		exit 1
	fi
	trap exitTest INT HUP STOP KILL
	mv "$WRAPPER" "$WRAPPER.bak"
	cp -p "$(dirname "$0")"/"$(basename "$WRAPPER")" $WRAPPER
	mv "$FUNCTIONS" "$FUNCTIONS.bak"
	cp -p "$(dirname "$0")"/"$(basename "$FUNCTIONS")" $FUNCTIONS
	QEMU_DUMMY="qemu-test-dummy.exe"
	source /etc/qemu-integration.conf
	QEMU_EXE="$( find "$QEMU_BIN_DIR" "$SYSROOT/bin" "$DIST" -name "qemu*.exe" 2> /dev/null | head -n1 )"
	if [ -z "$QEMU_EXE" ]
	then
		if [ -d "$QEMU_BIN_DIR" ]
		then
			QEMU_EXE="$QEMU_BIN_DIR/$QEMU_DUMMY"
		else
			QEMU_EXE="$SYSROOT/bin/$QEMU_DUMMY"
		fi
		mkdir -p "$(dirname "$QEMU_EXE")"
		touch "$QEMU_EXE"
	fi
	qemu-integration-update.sh
	QEMU="$(basename "$QEMU_EXE")"
}

function exitTest {
	if [ -n "$QEMU_EXE" ]
	then
		if [ "$QEMU_DUMMY" == "$(basename "$QEMU_EXE")" ]
		then
			rm "$QEMU_EXE"
		fi
		mv "$WRAPPER.bak" "$WRAPPER"
		mv "$FUNCTIONS.bak" "$FUNCTIONS"
		qemu-integration-update.sh
	fi
	exit $1
}

function winPath {
	local CYGPATH="$1"
	cygpath -w -m "$CYGPATH"
}

function isHome {
	[ "$CURRENT_TEST_DIR" == "$HOME_TEST_DIR" ]
}

function isTmp {
	[ "$CURRENT_TEST_DIR" == "$TMP_TEST_DIR" ]
}

function prepareParameterFile {
	local PFILE="$1"
	if [[ $PFILE =~ ^$HOME/ ]]
	then
		local OFFSET=$(( ${#HOME}+1 ))
		echo "~/${PFILE:$OFFSET}"
	else
		echo "$PFILE"
	fi
}

function relInHomeOnly {
	return 1
}

function qtestPrepare {
	mkdir -p "$CURRENT_TEST_DIR" && cd "$CURRENT_TEST_DIR" || exitTest 1
	[ -d "$DIR" ] && ( rm -r "$DIR" || exitTest 1 )
	mkdir -p "$DIR" || exitTest 1
}

function qtestEvaluate {
	local PPFILE=$(prepareParameterFile "$PARAMETER_FILE")
	infoLog
	infoLog "$DIR"
	infoLog "  WORKING_DIR:"
	infoLog "    '$(pwd)'"
	if [ -n "$LINKED_FILE" ]
	then
		infoLog "  LINKED_FILE:"
		infoLog "    '$LINKED_FILE'"
	fi
	infoLog "  PARAMETER_FILE:"
	infoLog "    '$PPFILE'"
	infoLog "  TRANSLATED_FILE:"
	infoLog "    '$TRANSLATED_FILE'"

	local WRAPPED_DUMP="$DIR/wrapped.dmp"
	local TESTED_DUMP="$DIR/tested.dmp"

	infoLog "--------------------------------------------------------------------------------"
	export QEXEC_DUMP="$WRAPPED_DUMP"
	infoLog "Assemble input commandline, execute and dump generated output commandline in"
	infoLog "$QEXEC_DUMP"
	infoLog
	testCmd "" "$QEMU" "$PPFILE"
	infoLog
	infoLog "--------------------------------------------------------------------------------"
	export QEXEC_DUMP="$TESTED_DUMP"
	infoLog "Assemble expected output commandline and dump it in"
	infoLog "$QEXEC_DUMP"
	testCmd "qexec" "$QEMU_BIN_DIR/$QEMU" "$TRANSLATED_FILE"
	infoLog "--------------------------------------------------------------------------------"

	infoLog
	if [ -f "$WRAPPED_DUMP" ] && [ -f "$TESTED_DUMP" ]
	then
		if cmp "$WRAPPED_DUMP" "$TESTED_DUMP"
		then
			rm -r "$DIR"
			infoLog "OK"
			infoLog
			infoLog "================================================================================"
			return
		else
			infoLog "NOK"
			infoLog
			>&2 diff "$WRAPPED_DUMP" "$TESTED_DUMP"
		fi
	else
		infoLog "Error: WRAPPED_DUMP='$WRAPPED_DUMP'"
	fi
	infoLog
	ALL_OK="false"
	infoLog "================================================================================"
}

function testCmd {
	local EXEC="$1" QEMU="$2" FILE="$3" TESTSTART TESTEND
	[ -z "$EXEC" ] && TESTSTART=$(date +%s%N)
	$EXEC "$QEMU" \
		-M q35 \
		-accel whpx,kernel-irqchip=off \
		-m 1536 \
		-pidfile .qemupid.1653717533 \
		-audiodev id=audio0,driver=dsound,in.voices=0 \
		-device ich9-intel-hda \
		-device hda-micro,audiodev=audio0 \
		-vga qxl \
		-spice port=5905,addr=127.0.0.1,disable-ticketing=on \
		-device virtio-serial \
		-chardev spicevmc,id=spicechannel0,name=vdagent \
		-device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 \
		-drive "logfile=$FILE,media=disk" \
		-drive "filename=$FILE,media=disk" \
		-drive "file.filename=$FILE,media=disk" \
		-drive "path=$FILE,media=disk" \
		-drive "splash=$FILE,media=disk" \
		-drive "any=none,script=$FILE" \
		-drive "any=none,x509-file=$FILE,media=disk" \
		-cdrom "$FILE" \
		-drive "file=$FILE,driver=raw" \
		-drive "file.filename=$FILE,file.driver=file,driver=raw" \
		'json:{
		  "file.filename":"'"$FILE"'",
		  "file.driver":"file",
		  "driver":"raw"
		 }' \
		'json:{
		  "file":{
		   "filename":"'"$FILE"'",
		   "driver":"file"
		  },
		  "driver":"raw"
		 }' \
		-o backing_file='json:{
		  "file.driver"   :  "https" , ,
		  "file.url":"https://user:password@vsphere.example.com/folder/test/test-flat.vmdk?dcPath=Datacenter&dsName=datastore1",,
		  "file.sslverify":  "off" , ,
		  "file.readahead":  "64k" , ,
		  "file.timeout"  :  10 , ,
		 }' \
		'json:{"":[],"":{},"":[ ],"":{ },"":[,],"":{,},"":[ , ],"":{ , },"":[,,,],"":{,,,}}' \
		'json:{
		  "driver"	: "qcow2",
		  "file"	: {
		   "driver"	 : "gluster",
		   "volume"	 : "testvol",
		   "path"	 : "a.img",
		   "debug"	 : 9,
		   "logfile"	 : "'"$FILE"'",
		   "server"	 : [
		    {
		     "type"	  : "tcp",
		     "host"	  : "1.2.3.4",
		     "port" 	  : 24007
		    },
		    {
		     "type"	  : "unix",
		     "socket"	  : "/var/run/glusterd.socket"
		    }
		   ]
		  }
		 }'
	[ -z "$EXEC" ] && TESTEND=$(date +%s%N)
	[ -z "$EXEC" ] && TESTDURATIONS+=( $(( (TESTEND-TESTSTART)/1000000 )) )
}

#########################################################
#
# qtest0X - relatively addressed link
# Parameter file relative: translate relative if possible
#

# Linked file in subdir is addressable relatively
function qtest00 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local REL_LINKED_FILE="$DIR/test a/abs test file"
	local LINKED_FILE="$CURRENT_TEST_DIR/$REL_LINKED_FILE"
	local PARAMETER_FILE="$DIR/rel test link"
	mkdir -p "$(dirname "$LINKED_FILE")"
	touch "$LINKED_FILE"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"

	local TRANSLATED_FILE="$REL_LINKED_FILE"
	relInHomeOnly && isTmp && TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate
}

# Linked file is addressable relatively
function qtest01 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local LINKED_FILE="/usr/share/doc/qemu-integration/LICENSE"
	local PARAMETER_FILE="$DIR/rel test link"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"

	local TRANSLATED_FILE="../..$LINKED_FILE"
	isHome && TRANSLATED_FILE="../$TRANSLATED_FILE"
	relInHomeOnly && TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate
}

# Linked file is NOT addressable relatively
function qtest02 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local LINKED_FILE="/cygdrive/c/Windows/win.ini"
	local PARAMETER_FILE="$DIR/rel test link"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"

	local TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate
}

# Linked file is in subdirectory of level1-superdirectory
function qtest03 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local REL_LINKED_FILE="../${REL_STD_TEST_DIR}2/test file"
	local LINKED_FILE="$CURRENT_TEST_DIR/$REL_LINKED_FILE"
	local PARAMETER_FILE="$DIR/rel test link"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"
	mkdir -p "$(dirname "$LINKED_FILE")"
	touch "$LINKED_FILE"

	local TRANSLATED_FILE="$REL_LINKED_FILE"
	relInHomeOnly && isTmp && TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate
}

# Linked file is in subdirectory of level2-superdirectory
function qtest04 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local REL_LINKED_FILE="../../${REL_STD_TEST_DIR}2/test file"
	local LINKED_FILE="$CURRENT_TEST_DIR/$REL_LINKED_FILE"
	local PARAMETER_FILE="$DIR/rel test link"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"
	mkdir -p "$(dirname "$LINKED_FILE")"
	touch "$LINKED_FILE"

	local TRANSLATED_FILE="$REL_LINKED_FILE"
	relInHomeOnly && TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate

	rm "$TRANSLATED_FILE"
	rmdir "$(dirname "$TRANSLATED_FILE")"
}

# Linked file is in subdirectory of outofbound-level-superdirectory (far super of / !)
# Link resolution is adjusted by Cygwin/Shell/?
function qtest05 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local REL_LINKED_FILE="../../../../../${REL_STD_TEST_DIR}2/test file"
	local LINKED_FILE="$CURRENT_TEST_DIR/$REL_LINKED_FILE"
	local PARAMETER_FILE="$DIR/rel test link"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"
	mkdir -p "$(dirname "$LINKED_FILE")"
	touch "$LINKED_FILE"

	local TRANSLATED_FILE="../../${REL_STD_TEST_DIR}2/test file"
	isHome && TRANSLATED_FILE="../$TRANSLATED_FILE"
	relInHomeOnly && TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate

	rm "$TRANSLATED_FILE"
	rmdir "$(dirname "$TRANSLATED_FILE")"
}

# Linked file is relative to link in superdirectory
# Linked file is NOT relative to working directory
function qtest06 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local LINKED_FILE="$DIR/test a/rel test file"
	local PARAMETER_FILE="$DIR/abs test link"
	(
		cd "$(dirname "$PARAMETER_FILE")"
		mkdir -p "$(dirname "$LINKED_FILE")"
		touch "$LINKED_FILE"
	)
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"

	local TRANSLATED_FILE="$(dirname "$PARAMETER_FILE")/$LINKED_FILE"

	qtestEvaluate
}

#########################################################
#
# qtest1X - absolutely addressed link
# Parameter file absolute: translate absolute
#

# Linked file is in subdirectory
function qtest10 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local LINKED_FILE="$CURRENT_TEST_DIR/$DIR/test a/abs test file"
	local PARAMETER_FILE="$CURRENT_TEST_DIR/$DIR/abs test link"
	mkdir -p "$(dirname "$LINKED_FILE")"
	touch "$LINKED_FILE"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"

	local TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate
}

# Linked file is absolute in completely different tree
function qtest11 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local LINKED_FILE="/usr/share/doc/qemu-integration/LICENSE"
	local PARAMETER_FILE="$CURRENT_TEST_DIR/$DIR/abs test link"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"

	local TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate
}

# Linked file is absolute outside cygwin-scope
function qtest12 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local LINKED_FILE="/cygdrive/c/Windows/win.ini"
	local PARAMETER_FILE="$CURRENT_TEST_DIR/$DIR/abs test link"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"

	local TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate
}

# Linked file is relative to link in subdirectory of superdirectory
function qtest13 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local REL_LINKED_FILE="../${REL_STD_TEST_DIR}2/test file"
	local LINKED_FILE="$CURRENT_TEST_DIR/$REL_LINKED_FILE"
	local PARAMETER_FILE="$CURRENT_TEST_DIR/$DIR/abs test link"
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"
	mkdir -p "$(dirname "$LINKED_FILE")"
	touch "$LINKED_FILE"

	local TRANSLATED_FILE="$(winPath "$LINKED_FILE")"

	qtestEvaluate
}

# Linked file is relative to link in superdirectory
# Linked file is NOT relative to working directory
function qtest14 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local LINKED_FILE="$DIR/test a/rel test file"
	local PARAMETER_FILE="$CURRENT_TEST_DIR/$DIR/abs test link"
	(
		cd "$(dirname "$PARAMETER_FILE")"
		mkdir -p "$(dirname "$LINKED_FILE")"
		touch "$LINKED_FILE"
	)
	ln -s "$LINKED_FILE" "$PARAMETER_FILE"

	local TRANSLATED_FILE="$(winPath "$(dirname "$PARAMETER_FILE")/$LINKED_FILE")"

	qtestEvaluate
}

#########################################################
#
# qtest2X - relatively addressed file
# Parameter file relative: translate relative
#

# File is in subdirectory
function qtest20 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="$DIR/rel test file"
	touch "$PARAMETER_FILE"

	local TRANSLATED_FILE="$PARAMETER_FILE"

	qtestEvaluate
}

# File is addressed in subdirectory, but doesn't yet exist
function qtest21 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="$DIR/rel test file"

	local TRANSLATED_FILE="$PARAMETER_FILE"

	qtestEvaluate
}

# File is in not normalized subdirectory
function qtest22 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="../${REL_STD_TEST_DIR}/$DIR/rel test file"
	mkdir -p "$(dirname "$PARAMETER_FILE")"
	touch "$PARAMETER_FILE"

	local TRANSLATED_FILE="$DIR/rel test file"

	qtestEvaluate
}

# File is addressed not normalized subdirectory, but doesn't yet exist
function qtest23 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="../${REL_STD_TEST_DIR}/$DIR/rel test file"
	mkdir -p "$(dirname "$PARAMETER_FILE")"

	local TRANSLATED_FILE="$DIR/rel test file"

	qtestEvaluate
}

# File is in subdirectory of super directory
function qtest24 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="../${REL_STD_TEST_DIR}2/$DIR/rel test file"
	mkdir -p "$(dirname "$PARAMETER_FILE")"
	touch "$PARAMETER_FILE"

	local TRANSLATED_FILE="$PARAMETER_FILE"

	qtestEvaluate
}

# File is addressed subdirectory of super directory, but doesn't yet exist
function qtest25 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="../${REL_STD_TEST_DIR}2/$DIR/rel test file"
	mkdir -p "$(dirname "$PARAMETER_FILE")"

	local TRANSLATED_FILE="$PARAMETER_FILE"

	qtestEvaluate
}

#########################################################
#
# qtest3X - absolutely addressed file
# Parameter file absolute: translate absolute
#

# File is in subdirectory
function qtest30 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="$CURRENT_TEST_DIR/$DIR/abs test file"
	touch "$PARAMETER_FILE"

	local TRANSLATED_FILE="$(winPath "$PARAMETER_FILE")"

	qtestEvaluate
}

# File is in subdirectory, but doesn't yet exist
function qtest31 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="$CURRENT_TEST_DIR/$DIR/abs test file"

	local TRANSLATED_FILE="$(winPath "$PARAMETER_FILE")"

	qtestEvaluate
}

#########################################################
#
# qtest4X - windows style addressed file
# No translation
#

#
function qtest40 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="c:\\$DIR\\abs test file"
	local TRANSLATED_FILE="$PARAMETER_FILE"

	qtestEvaluate
}

#
function qtest41 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="\\$DIR\\abs test file"
	local TRANSLATED_FILE="$PARAMETER_FILE"

	qtestEvaluate
}

#
function qtest42 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="$DIR\\abs test file"
	local TRANSLATED_FILE="$PARAMETER_FILE"

	qtestEvaluate
}

#
function qtest43 {
	local DIR="test ${FUNCNAME[0]}"
	qtestPrepare

	local PARAMETER_FILE="c:/$DIR/abs test file"
	local TRANSLATED_FILE="$PARAMETER_FILE"

	qtestEvaluate
}

setupTest
declare -a TESTDURATIONS
START=$(date +%s%N)
WORKING_DIRS="$TMP_TEST_DIR $HOME_TEST_DIR"
ALL_OK="true"
for CURRENT_TEST_DIR in $WORKING_DIRS
do
	if (( 0 < $# ))
	then
		I=1
		while (( I <= $# ))
		do
			"${!I}"
			I=$(( I+1 ))
		done
	else
		qtest00
		qtest01
		qtest02
		qtest03
		qtest04
		qtest05
		qtest06
		qtest10
		qtest11
		qtest12
		qtest13
		qtest14
		qtest20
		qtest21
		qtest22
		qtest23
		qtest24
		qtest25
		qtest30
		qtest31
		qtest40
		qtest41
		qtest42
		qtest43
	fi
done
infoLog
END=$(date +%s%N)
echo "Performing tests took $(( (END-START)/1000000 ))ms"
TDALL=0
for TD in "${TESTDURATIONS[@]}"
do
	traceLog $TD
	( [ -z "$TDMIN" ] || (( TDMIN > TD )) ) && TDMIN=$TD
	( [ -z "$TDMAX" ] || (( TDMAX < TD )) ) && TDMAX=$TD
	TDALL=$(( TD+TDALL ))
done
echo "Fastest $(( TDMIN ))ms, Slowest $(( TDMAX ))ms, Average $(( TDALL/${#TESTDURATIONS[@]} ))ms"
[ "$ALL_OK" == "true" ] && infoLog "PASS - All tests passed." || infoLog "FAIL - One or more tests failed."
infoLog
exitTest
