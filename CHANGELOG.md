ChangeLog
=========

Changes in 1.3.0:
-----------------
* Adds path resolution for json parameters
* More tests, tests covering json parameters
* Optimizations: less sub processes, more bash-internal

Changes in 1.2.1:
-----------------
* Automated tests
* Bugfixes
** Relative path resolution

Changes in 1.2.0:
-----------------
* Better visualization of translation message

Changes in 1.1.1:
-----------------
* Bugfixes
** replace echo with echoParams to print even echo-options like "-n" to STDOUT
** Recognize and translate file.filename
* More trace-logging

Changes in 1.1.0:
-----------------
* Add also autodetection for Qemu available from https://qemu.weilnetz.de

Major Changes in 1.0.0:
-----------------------
First release.

* Integration of Qemu binaries, man pages and firmware descriptors
* Path translation of Unix paths to Windows paths
